# Git Productivity

Just a collection of aliases I use in git for productivity sake

# Documentation

## `git glog` - "Graphical Log"

This is a alias for a clean graphical log, the output is one-line, with reference decorators 
and a graph layout with date time of commit in the output as well.

Usage:

```
git glog <git-log parameters>
```

## `git dlog` - "Decorated Log"

This is an alias for a decorated, graphical one line log, this is mostly just a keystroke saver and doesnt
add anything new that the standard cli options can't provide.

Usage:

```
git dlog <git-log parameters>
```

## `git last` - "Show last"

This shows you the details of the last commit on this branch.

Usage:

```
git last
```

## `git unpushed` - "Show unpushed commits"

This shows you the commits on your branch that haven't been pushed to the tracked remote reference. Obviously
requires that the current branch is tracked on a remote to work.

Usage:

```
git unpushed
```

## `git oops` - "Oops, commit"

This simply commits the staged changes to the last commit, often used when you noticed something you forgot
to include in the previous commit.

Usage:

```
git oops
```

## `git fixup` - "Fixup a previous commit"

This is used to mark a commit as a fixup for a previous commit. Often if you miss commiting something and
it was not part of the last commit but one further back in the chain, you can use this to mark for moving &
squashing in a later rebase step.

Usage:

```
git fixup <ref>
```

## `git st` - "Status; Simplified"

This is just a shortcut for a more consise `git status`

Usage:

```
git st <git-status parameters>
```

## `git pop` - "Undo"

This is used to undo the last commit, and leave it's changes in the working directory.

Usage:

```
git pop
```

## `git cd` - "Change the date"

This is used when you forget to commit something and you want the record to show when it was relevant
this will ammend the commit and change both the COMMITED DATE and AUTHORED DATE. Mostly used for telling
a better story with your git history.

Usage:

```
git cd "<quoted iso date/time>"
```

## `git prune-branches` - "Remove the trash"

This will remove any local branches that tracked a remote branch that no longer exists. Used to keep your local
branch store lean. Will not affect branches that are not tracked.

Usage:

```
git prune-branches
```

## `git asq` - "Squash Squash baby!"

This will perform an interactive rebase with the autosquash option, mostly just a keystroke saver, but quite 
useful. See `git fixup` for more details.

Usage:

```
git asq <rebase parameters>
```

## `git pun` - "Push a new branch"

This will push a new branch and automatically track it, mostly a keystroke saver, but also helps keep the 
habit of tracking branches on the remote.

Usage:

```
git pun
```

## `git pu` - "Push, duh!"

This mostly just keeps things in the same namespacing as the other push commands, largely laziness.

Usage:

```
git pu
```

## `git puf` - "Force push, but safely!"

This will do a force push, but it will use the optional lease system to make sure the force push doesn't
clobber someone else working in that remote at the same time. Just a safer version of force push.

```
git puf
```

## `git su` - "Stage Untracked"

This will stage all untracked files with intent to add but no content. Check with git st/status afterwards 
and remove anything extra.

```
git su
```

