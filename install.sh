#!/usr/bin/env bash

####################################
# Git Productivity Aliases Installer


# Uninstall aliases
git config --global --unset-all alias.glog
git config --global --unset-all alias.dlog
git config --global --unset-all alias.last
git config --global --unset-all alias.unpushed
git config --global --unset-all alias.oops
git config --global --unset-all alias.fixup
git config --global --unset-all alias.st
git config --global --unset-all alias.pop
git config --global --unset-all alias.cd
git config --global --unset-all alias.prune-branches
git config --global --unset-all alias.asq
git config --global --unset-all alias.pun
git config --global --unset-all alias.pu
git config --global --unset-all alias.puf
git config --global --unset-all alias.su

# Install aliases
git config --global alias.glog            'log --oneline --graph --pretty=format:"%C(yellow)%h %C(blue)%ai %Creset%s %C(red)%d"'
git config --global alias.dlog            'log --oneline --graph --decorate'
git config --global alias.last            'log -1 --oneline --color'
git config --global alias.unpushed        'log @{u}..'
git config --global alias.oops            'commit --amend --no-edit'
git config --global alias.fixup           'commit --fixup'
git config --global alias.st              'status -sb'
git config --global alias.pop      		    'reset HEAD^'
git config --global alias.cd       		    '!f() { GIT_COMMITTER_DATE=$1 git commit --amend --date $1; }; f'
git config --global alias.prune-branches 	'!f() { git remote prune origin && git branch -vv | grep '"'"': gone]'"'"'| awk '"'"'{print $1}'"'"' | xargs -r git branch -d; }; f'
git config --global alias.asq             'rebase -i --autosquash'
git config --global alias.pun           'push -u origin HEAD'
git config --global alias.pu            'push'
git config --global alias.puf           'push --force-with-lease'
git config --global alias.su		'!f() { git status -su ${1:-} | grep "??" | cut -d " " -f 2 | xargs -d "\n" -t git add -N; }; f'

echo "Aliases Installed to git global config"
